﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface ITemplateRepository<T> where T : class
    {
        IEnumerable<T> ItemCollection { get; }
        void Create(T item);
        void Update(T item);
        T Details(int id);
        void Delete(T item);
        void Delete(int id);

    }
}
