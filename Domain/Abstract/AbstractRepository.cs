﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Concrete;

namespace Domain
{
    public abstract class AbstractRepository<T> : ITemplateRepository<T> where T : class
    {
        protected EfDbContext _db = new EfDbContext();
        private DbSet dbSet = new EfDbContext().Set<T>();


        public IEnumerable<T> ItemCollection
        {
            get { return (IEnumerable<T>) dbSet; }
        }

        public abstract void Create(T item);

        public abstract void Update(T item);
        public abstract void Delete(int id);

        public T Details(int id)
        {
            return (T) dbSet.Find(id);
        }

        public void Delete(T item)
        {
            dbSet.Remove(item);
            _db.SaveChanges();
        }
    }
}
