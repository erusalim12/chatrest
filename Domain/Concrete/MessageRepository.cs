﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entity;

namespace Domain.Concrete
{
   public class MessageRepository:AbstractRepository<Message>
    {
       public override void Create(Message item)
       {
           _db.Messages.Add(item);
           _db.SaveChanges();
       }

       public override void Update(Message item)
       {
           var founded = _db.Messages.Find(item.Id);
           if (founded != null)
           {
               founded.Text = item.Text;
           }
           _db.SaveChanges();
       }

        public override void Delete(int id)
        {
            var founded = _db.Messages.Find(id);
            if(founded!=null)
            _db.Messages.Remove(founded);
            _db.SaveChanges();
        }
    }
}
