﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entity;

namespace Domain.Concrete
{
    public class UserRepository:AbstractRepository<User>
    {
        public override void Create(User item)
        {
            _db.Users.Add(item);
            _db.SaveChanges();
        }

        public override void Update(User item)
        {
            var founded = _db.Users.Find(item.Id);
            if (founded != null)
            {
                founded.Login = item.Login;
                founded.Password = item.Password;
            }
            _db.SaveChanges();
        }

        public override void Delete(int id)
        {
            var founded = _db.Users.Find(id);
            if (founded != null)
                _db.Users.Remove(founded);
            _db.SaveChanges();
        }
    }
}
