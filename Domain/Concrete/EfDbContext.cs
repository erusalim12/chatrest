﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entity;


namespace Domain.Concrete
{
    public class EfDbContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }


        public EfDbContext() : base("EfDbContext")
        {
            Database.CreateIfNotExists();
        }
    }
    
}
