﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entity
{

    public class Message
    {
        public Message()
        {
            Date = DateTime.Now.ToShortDateString();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Текст")]
        public string Text { get; set; }

     
        public virtual User Author { get; set; }

        [ForeignKey("Author")]
        public int? UserId { get; set; }
        public string Date { get; private set; }


        public string FilePath { get; set; }
        public string Mime { get; set; }
        public string Extension { get; set; }
    }
}

