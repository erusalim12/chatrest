﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entity
{
    public class User
    {
        public User()
        {
            Messages = new List<Message>();
        }

        public int Id { get; set; }
        public string Login { get; set; }

        public string Password { get; set; }


        public virtual ICollection<Message> Messages { get; set; }

    }
}
