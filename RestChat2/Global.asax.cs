﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Domain.Concrete;
using RestChat2.App_Start;

namespace RestChat2
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer<EfDbContext>(null);
            
            
        }
        protected void Application_EndRequest()
        {
            if (HttpContext.Current.Items["login"] != null)
            {
                Context.Response.Cookies.Remove(System.Web.Security.FormsAuthentication.FormsCookieName);
            }
        }
    }
}
