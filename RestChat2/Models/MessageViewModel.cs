﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Concrete;
using Domain.Entity;

namespace RestChat2.Models
{
    public class MessageViewModel
    {
        public MessageViewModel(Message message)
        {
            Id = message.Id;
            Text = message.Text;
            FilePath = message.FilePath;

            if (message.UserId != null)
            {
                Author = new UserRepository().ItemCollection.FirstOrDefault(u => u.Id == message.UserId.Value).Login;
            }
            else
            {
                Author = "автора нет";
            }
            Date = message.Date ?? DateTime.Now.ToLongDateString();
        }

        public int Id { get;private set; }
        public string Text { get; private set; }
        public string FilePath { get; private set; }
        public string Author { get;  private set; }
        public string Date { get; private set; }

    }


}