﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Concrete;
using Domain.Entity;
using Moq;
using Ninject;

namespace RestChat2.Infrastructure
{
    public class NinjectDependencyResolver:IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            addBindings();
        }

        private void addBindings()
        {
            kernel.Bind<ITemplateRepository<User>>().To<UserRepository>();
            kernel.Bind<ITemplateRepository<Message>>().To<MessageRepository>();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

       
    }
}