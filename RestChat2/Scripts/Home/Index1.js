﻿var urlPath = "http://localhost:1263/api/";
//   selectView(
function selectView(view) {
    $('.display').not('#' + view + "Display").hide();
    $('#' + view + "Display").show();
}
var selected = 0;
//получение всех сообщений с сервера и запись в таблицу
function getData() {
    $.ajax({
        type: "GET",
        url: urlPath + "Messaging",
        success: function (data) {
            $('#tableBody').empty();
            for (var i = 0; i < data.length; i++) {
                $('#tableBody').append('<tr><td>'
                    + data[i].Id + '</td>'
                    + '<td>' + data[i].Author + '</td>'
                    + '<td>' + data[i].Text + '</td>'
                    + '<td><a href="' + data[i].Id + '">Файл</a>'
                    + '<td>' + data[i].Date + '</td></tr>');
            };
            $('#tableBody tr').click(function () {

                $('.highlight').removeClass('highlight');
                $(this).addClass("highlight");

                selected = $(this).find('td:first').text();

                //alert("записали ID = " + selected);
            });
        }

    });
 
}



//проверка авторизации
function isAuthirized(nameFromCookie) {
    if (nameFromCookie != null) {
        selectView("summary");
        $("#username").text($.cookie("login"));
        getData();
    } else {
        selectView("Login");
    }
}



//действия при загрузке и события по нажатиям кнопок
$(document).ready(function () {

    $("a").click(function () {
        event.preventDefault();
        $.ajax({
            url: '/api/file/' + $(this).text(),
            type: "GET",
            dataType: "JSON",
            //data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                //   alert(data);
                getData();
            },
            error: function (xhr, desc, err) {
                alert(err);
            }
        });
    });

    //выделение строки в таблице
    $('#tableBody tr').click(function () {

        $('.highlight').removeClass('highlight');
        $(this).addClass("highlight");

        selected = $(this).find('td:first').text();
        alert("записали ID = " + selected);
    });

    //проверка авторизации
    isAuthirized($.cookie('login'));
    //если выторизован, то загружается таблица с данными

    /////////////////////////////////
    $("button").click(function (e) {
        switch (e.target.id) {


            case "refresh"://работает
                getData();
                break;

            case "delete"://работает
                $.ajax({
                    type: "DELETE",
                    url: urlPath + "Messaging/" + selected,
                    success: function (data) {
                        $('#tableBody tr').filter(function () {
                            return this.textContent.trim() === selected;
                        }).remove();
                        getData();
                    }
                });

                break;

            case "add":
                $("#editPanel").addClass("hidden");
                $("#createPanel").removeClass("hidden");
                $("#form2 #oldFile").remove();
                document.getElementById("form1").reset();
                document.getElementById("form2").reset();
              
                break;

            case "edit":
                if (selected !== 0) {
                    $.ajax({
                        type: "GET",
                        url: urlPath + "Messaging/" + selected,
                        success: function(data) {
                            $("#createPanel").addClass("hidden");
                            $("#editPanel").removeClass("hidden");
                            $("#form2 #oldFile").remove();
                            $('#IdBox').val(data.Id);
                            $('#Text1').val(data.Text);
                            if (data.FilePath != null || data.FilePath != "")
                                $("#form2").append('<a id="oldFile"  href=' +data.Id  + '>Файл</a>');
                        },
                        error: function(data) {
                            alert(data);
                        }
                    });
                }
                break;

           

            case "Logout":
                $.ajax({
                    type: "GET",
                    url: urlPath + "Authorization/",
                    data: $.cookie('login'),
                    success: function (result) {
                        alert("Успешно вышел");
                        selectView("Login");
                    }

                });
                break;

            case "login":
                $.ajax({
                    type: "GET",
                    url: urlPath + "Authorization/",
                    data: $("#LoginForm").serialize(),
                    success: function (result) {
                        $("#username").text($.cookie('login'));
                        alert("Добро пожаловать, " + $.cookie('login'));
                        if (true) {
                            selectView("summary");
                        }
                    },
                    error: function () {
                        alert("Ошибка авторизации");
                    }

                });
                break;

        }
    });

});
