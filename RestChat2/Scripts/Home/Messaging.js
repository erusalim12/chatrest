﻿var urlPath = "http://localhost:1263/api/";
//   selectView(
function selectView(view) {
    $('.display').not('#' + view + "Display").hide();
    $('#' + view + "Display").show();
}
var selected = -1;

function getData() {
    $.ajax({
        type: "GET",
        url: urlPath + "Messaging",
        success: function(data) {
            $('#tableBody').empty();
            for (var i = 0; i < data.length; i++) {
                $('#tableBody').append('<tr><td>'
                    + data[i].Id + '</td>'
                    + '<td>' + data[i].Author + '</td>'
                    + '<td>' + data[i].Text + '</td>'
                    + '<td><img src="'+  data[i].FilePath + '"/>'
                    + '<td>' + data[i].Date + '</td></tr>');
            };

            $('#tableBody tr').click(function () {

                $('.highlight').removeClass('highlight');
                $(this).addClass("highlight");

                selected = $(this).find('td:first').text();

                alert("записали ID = " + selected);
            });
        }
    });
}

///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
function isAuthirized (nameFromCookie) {
    if (nameFromCookie != null) {
        selectView("autorized");
    }
    selectView("Login");
}

$(document).ready(function () {
    
 isAuthirized($.cookie('login'));

      getData();
    /////////////////////////////////
    $("button").click(function (e) {
        switch (e.target.id) {
            case "refresh"://работает
                getData();
                break;

            case "delete"://работает
                $.ajax({
                    type: "DELETE",
                    url: urlPath + "Messaging/" + selected,
                    success: function (data) {
                         $('#tableBody tr').filter(function () {
                            return this.textContent.trim() === selected;
                        }).remove();
                         getData();
                        // $('#tableBody tr:eq(selected)').remove();
                    }
                });
              
                break;
               
            case "add":
                document.getElementById("form1").reset();
                $.cookie('messageId', null);
                break;

            case "edit":
                $.ajax({
                    type: "GET",
                    url: urlPath + "Messaging/" + selected,
                    success: function (data) {
                        $.cookie('messageId', data.Id);
                      //  $('#IdBox').val(data.Id);
                        $('#Text').val(data.Text);
                        $('#File').val(data.FilePath);
                    }
                });
                break;


            case "login":
                $.ajax({
                    type: "GET",
                    url: urlPath + "Authorization/",
                    data: $("#LoginForm").serialize(),
                    success: function (result) {
                        $("#username").text($.cookie('login'));
                        alert("Добро пожаловать, " +$.cookie('login'));
                        if (true) {
                            selectView("summary");
                        }
                    },
                    error: function () {
                        alert("Ошибка авторизации");
                    }

                });
                break;

            case "Logout":
                $.ajax({
                    type: "GET",
                    url: urlPath + "Authorization/",
                    data: $.cookie('login'),
                    success: function (result) {
                        alert("Успешно вышел");
                        selectView("Login");
                    }

                });
                break;
        }
    });
  
});


