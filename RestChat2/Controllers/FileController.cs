﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Http;
using Domain;
using Domain.Concrete;
using Domain.Entity;

namespace RestChat2.Controllers
{
    public class FileController : ApiController
    {
        private ITemplateRepository<Message> _repository = new MessageRepository();

        public HttpResponseMessage GetFile(int id)
        {
            var elem = _repository.Details(id);
            var path = HttpContext.Current.Server.MapPath(elem.FilePath);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);

          //  result.Headers.Add("content-disposition", "attachment; filename = \""+elem.Extension+"\"");

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = elem.Extension
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue(elem.Mime);

            return result;
        }

    }
}
