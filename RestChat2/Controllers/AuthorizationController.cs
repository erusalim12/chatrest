﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Web.UI.WebControls;
using Domain;
using Domain.Concrete;
using Domain.Entity;
using RestChat2.Filters;


namespace RestChat2.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class AuthorizationController : ApiController
    {      
        private ITemplateRepository<User> _repository =new UserRepository();

        //логин
        public HttpResponseMessage Get(string Login, string Password)
        {
            User founded = _repository.ItemCollection.FirstOrDefault(u => u.Login == Login);
            if (founded != null)
            {
                if (founded.Password == Password)
                {
                    var resp = new HttpResponseMessage();

                    var cookie = new CookieHeaderValue("login", Login)
                    {
                        Expires = DateTimeOffset.Now.AddDays(1),
                        Domain = Request.RequestUri.Host,
                        Path = "/"
                    };

                    resp.Headers.AddCookies(new CookieHeaderValue[] {cookie});
                    return resp;
                }
            }
            return null;
        }
        //логаут
 
        public HttpResponseMessage Get()
        {
                    var resp = new HttpResponseMessage();

                    var cookie = new CookieHeaderValue("login", "")
                    {
                        Expires = DateTimeOffset.Now.AddDays(-1),
                        Domain = Request.RequestUri.Host,
                        Path = "/"
                    };

                    resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                    return resp;
        }
       

        /*   [HttpGet]
         public HttpResponseMessage GetAutorizationToken(string Login, string Password)
          {
              User founded = _repository.ItemCollection.FirstOrDefault(u => u.Login == Login);
              if (founded != null)
              {
                  if (founded.Password == Password)
                  {
                      var vals = new NameValueCollection(); // using System.Collections.Specialized
                      vals["uId"] = founded.Id.ToString();
                      vals["Name"] = founded.Login;
                      var cookie = new CookieHeaderValue("user", vals);

                      var response = Request.CreateResponse(HttpStatusCode.OK,founded);
                      response.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                      return response;
                  }
              }
              return null;
          }*/
        /*
        [HttpGet]
        [NoResponseCookie]
        public void LogOut(string login)
        {
            string Name;
            string uId;
            CookieHeaderValue cookie = Request.Headers.GetCookies("user").FirstOrDefault();
            if (cookie != null)
            {
                CookieState cookieState = cookie["user"];
                uId = cookieState["uId"];
                Name = cookieState["Name"];
                if (login == Name)
                {
                    
                }
            }

        }*/



    }
}
