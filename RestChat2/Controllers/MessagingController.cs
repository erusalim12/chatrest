﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Antlr.Runtime;
using Domain;
using Domain.Concrete;
using Domain.Entity;
using Newtonsoft.Json;
using RestChat2.Models;

namespace RestChat2.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class MessagingController : ApiController
    {

        private ITemplateRepository<Message> _repository = new MessageRepository();

        [HttpGet]
        public IEnumerable<MessageViewModel> GetAllMessages()
        {
            List<MessageViewModel> Items = new List<MessageViewModel>();
            foreach (Message message in _repository.ItemCollection)
            {
                var mvm = new MessageViewModel(message);
                Items.Add(mvm);
            }
           
            return Items;

            
        }

        [HttpGet]
        public MessageViewModel GetMessage(int id)
        {
            var item =  _repository.ItemCollection.First(m => m.Id == id);
            var elem = new MessageViewModel(item);
            return elem;
        }

        [HttpDelete]
        public void DeleteMessage(int id)
        {
            _repository.Delete(id);
        }

        public async Task<IHttpActionResult> PostMessage()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body
                var message = new Message();
                // Read the form data and return an async task.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the form data.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        message.Text = val;
                    }
                }

                // This illustrates how to get the file names for uploaded files.
                foreach (var file in provider.FileData)
                {
                  //  FileInfo fileInfo = new FileInfo(file.LocalFileName);
                     FileInfo fileInfo = new FileInfo(file.LocalFileName);
                    message.Mime = file.Headers.ContentType.MediaType;
                    message.Extension = file.Headers.ContentDisposition.FileName;
                    message.FilePath = trimPath(fileInfo.DirectoryName + "\\" + fileInfo.Name);
                }

                CookieHeaderValue cookie = Request.Headers.GetCookies("login").FirstOrDefault();
                            if (cookie != null)
                            {
                                message.UserId = FindAuthor(cookie["login"].Value);
                            }

                _repository.Create(message);
                return Ok("файлы загружены");
            }
            catch (System.Exception e)
            {
                return BadRequest();
            }
        }


        public async Task<IHttpActionResult>PutMessage(int id)
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body
                var message = _repository.Details(id);
                // Read the form data and return an async task.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the form data.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        message.Text = val;
                    }
                }

                // This illustrates how to get the file names for uploaded files.
                foreach (var file in provider.FileData)
                {
                    //  FileInfo fileInfo = new FileInfo(file.LocalFileName);
                    FileInfo fileInfo = new FileInfo(file.LocalFileName);

                    message.FilePath = trimPath(fileInfo.DirectoryName + "\\" + fileInfo.Name + "." + fileInfo.Extension);
                }

                CookieHeaderValue cookie = Request.Headers.GetCookies("login").FirstOrDefault();
                if (cookie != null)
                {
                    message.UserId = FindAuthor(cookie["login"].Value);
                }

                _repository.Update(message);
                return Ok("файлы загружены");
            }
            catch (System.Exception e)
            {
                return BadRequest();
            }
        }


        private int? FindAuthor(string name)
        {
            var user = new UserRepository().ItemCollection.FirstOrDefault(l => l.Login == name);
            if (user != null)
            {
                return user.Id;
            }
            return null;
        }

        private string trimPath(string Path)
        {
            return Path.Substring(Path.IndexOf(@"\App_Data", StringComparison.Ordinal));
        }
    }
}
