﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace RestChat2.Filters
{
    public class NoResponseCookieAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            System.Web.HttpContext.Current.Items.Add("user", "true");
        }
    }

}